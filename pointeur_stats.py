import json
from datetime import datetime
from pathlib import Path


def format_time(seconds: int) -> str:
    return f"{seconds // 60}h {seconds % 60}m"


file = Path().cwd() / "hours.json"
with open(file, "r") as f:
    data = json.load(f)
    week = str(datetime.now().isocalendar()[1])
    if week not in data:
        exit(1)
    day = datetime.now().isocalendar()[2] - 1
    total_time = sum(data[week]["time"])
    print(f"Total time this week: {format_time(total_time)}")
    if total_time > 420 * (day + 1):
        print(f"You have an advance of {format_time(total_time - 420 * (day + 1))}.")
        print("Management is proud of you")
    else:
        print(f"You have a delay of {format_time(420 * (day + 1) - total_time)}.")
        print("Management is watching you!")
input("Press Enter to exit")
