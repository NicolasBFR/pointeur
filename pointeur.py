import json
from datetime import datetime
from pathlib import Path

path = Path().cwd()  # Add getenv("LOCALAPPDATA")
print(path)
path.mkdir(exist_ok=True, parents=True)

# If file "hours.json" does not exist, create it with empty json object
file = path / "hours.json"
if not file.exists():
    with open(file, "w") as f:
        json.dump({}, f, indent=4)
with open(file, "r") as f:
    data = json.load(f)

# Get current week number of year
week = str(datetime.now().isocalendar()[1])
print(week)
if week not in data:
    data[week] = {"lastClick": datetime.now().isoformat(), "time": [0] * 5}
    with open(file, "w") as f:
        json.dump(data, f, indent=4)
    exit(0)
day = datetime.now().isocalendar()[2] - 1
print(day)
if day >= 5:
    exit(1)
# If lastClick is not today, set lastClick to today
if datetime.fromisoformat(data[week]["lastClick"]).date() != datetime.now().date():
    data[week]["lastClick"] = datetime.now().isoformat()
    with open(file, "w") as f:
        json.dump(data, f, indent=4)
    exit(0)
answer = input("Increment time ?: ").lower()
if answer == "y" or answer == "ye" or answer == "yes":
    # Increment time by the number of minutes since lastClick time
    data[week]["time"][day] += (
                                       datetime.now() - datetime.fromisoformat(data[week]["lastClick"])
                               ).seconds // 60
data[week]["lastClick"] = datetime.now().isoformat()

with open(file, "w") as f:
    json.dump(data, f, indent=4)
